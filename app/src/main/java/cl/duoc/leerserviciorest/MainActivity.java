package cl.duoc.leerserviciorest;

import android.app.ProgressDialog;
import android.content.Context;
import android.os.AsyncTask;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.ListView;
import android.widget.Toast;

import cl.duoc.leerserviciorest.adapter.PersonaAdapter;
import cl.duoc.leerserviciorest.entidades.Persona;
import cl.duoc.leerserviciorest.ws.WSEjemplo;

public class MainActivity extends AppCompatActivity {

    Button btnRest;
    ListView listView;
    PersonaAdapter adapter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        btnRest = (Button) findViewById(R.id.btnRest);

        btnRest.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                consultarDatos();
            }
        });

        listView = (ListView)findViewById(R.id.lstPersonas);

    }

    private void consultarDatos() {
        new ObtieneDatos(this).execute();
    }


    private class ObtieneDatos extends AsyncTask<Void, Void, Persona[]>{

        private final Context context;
        private ProgressDialog mProgress;

        public ObtieneDatos(Context context){
            this.context = context;
        }


        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            mProgress = new ProgressDialog(context);
            mProgress.setIndeterminate(true);
            mProgress.setMessage("Obteniendo Información");
            mProgress.setCancelable(false);
            mProgress.show();

        }

        @Override
        protected Persona[] doInBackground(Void... params) {

            Persona[] values = null;
            try{

                values = WSEjemplo.getInstance().listadoPersonas();

            }catch (Exception e){

            }


            return values;
        }

        @Override
        protected void onPostExecute(Persona[] personas) {
            super.onPostExecute(personas);
            mProgress.hide();
            cargarInformacion(personas);
        }
    }

    private void cargarInformacion(Persona[] personas) {
        if(personas != null) {
            Toast.makeText(this, "Se cargaron " + personas.length + " personas", Toast.LENGTH_LONG).show();
            adapter = new PersonaAdapter(this, personas);
            listView.setAdapter(adapter);
        }
    }


}
