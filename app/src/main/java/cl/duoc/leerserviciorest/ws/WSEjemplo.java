package cl.duoc.leerserviciorest.ws;

import com.google.gson.Gson;
import com.squareup.okhttp.FormEncodingBuilder;
import com.squareup.okhttp.OkHttpClient;
import com.squareup.okhttp.Request;
import com.squareup.okhttp.RequestBody;
import com.squareup.okhttp.Response;

import java.io.IOException;
import java.util.concurrent.TimeUnit;

import cl.duoc.leerserviciorest.entidades.Persona;


/**
 * Created by salemlabs on 04-05-16.
 */
public class WSEjemplo {

    private static final long TIME_OUT_SECONDS = 5000;
    private static WSEjemplo instance;

    public static WSEjemplo getInstance() {
        if (instance == null) {
            instance = new WSEjemplo();
        }
        return instance;
    }

    private WSEjemplo(){}

    public Persona[] listadoPersonas(){
        Persona[] values = null;

        String url = "https://script.google.com/macros/s/AKfycbzRJWWPTorl-U63q7-jjZrMPLrD6QCGLzACP2DZjRrTgCdKS9zw/exec";
        try {
            String respuesta = getRequest(url);
            Gson gson = new Gson();
            values = gson.fromJson(respuesta, Persona[].class);

        } catch (Exception e) {

        }

        return values;
    }

/*
    public RespuestaPost registroPersona(Context context, Persona values) {
        RespuestaPost retorno = null;
        FormEncodingBuilder formBody = new FormEncodingBuilder()
                .add("id", values.getId())
                .add("nombre", values.getNombre())
                .add("apellido", values.getApellido())
                .add("rut", values.getRut());

        String url = context.getResources().getString(R.string.url_servicio);
        try {
            String respuesta = postRequest(url, formBody);

            final GsonBuilder gsonBuilder = new GsonBuilder();
            gsonBuilder.registerTypeAdapter(RespuestaPost.class, new AgregarPersonaGSON());
            final Gson gson = gsonBuilder.create();
            retorno = gson.fromJson(respuesta, RespuestaPost.class);
        } catch (Exception e) {
            e.printStackTrace();
        }
        return retorno;
    }

    public RespuestaGet listarPersonas(Context context) {
        RespuestaGet retorno = null;

        String url = context.getResources().getString(R.string.url_servicio);
        try {
            String respuesta = getRequest(url);

            final GsonBuilder gsonBuilder = new GsonBuilder();
            gsonBuilder.registerTypeAdapter(RespuestaGet.class, new ListarPersonaGSON());
            final Gson gson = gsonBuilder.create();
            retorno = gson.fromJson(respuesta, RespuestaGet.class);
        } catch (Exception e) {

        }
        return retorno;
    }*/

    private String postRequest(String url, FormEncodingBuilder params) throws IOException {
        OkHttpClient client = new OkHttpClient();
        client.setConnectTimeout(TIME_OUT_SECONDS, TimeUnit.SECONDS); // connect timeout
        client.setReadTimeout(TIME_OUT_SECONDS, TimeUnit.SECONDS);

        RequestBody formBody = params.build();

        Request request = new Request.Builder()
                .url(url)
                .post(formBody)
                .build();

        Response response = client.newCall(request).execute();
        if (!response.isSuccessful()) throw new IOException("Unexpected code " + response);
        String respuesta = response.body().string();
        return respuesta;
    }

    private String getRequest(String url)  throws IOException {
        OkHttpClient client = new OkHttpClient();
        client.setConnectTimeout(TIME_OUT_SECONDS, TimeUnit.SECONDS); // connect timeout
        client.setReadTimeout(TIME_OUT_SECONDS, TimeUnit.SECONDS);
        Request request = new Request.Builder()
                .url(url)
                .get()
                .addHeader("cache-control", "no-cache")
                .build();

        Response response = client.newCall(request).execute();
        if (!response.isSuccessful()) throw new IOException("Unexpected code " + response);
        String respuesta = response.body().string();
        return respuesta;
    }


}
